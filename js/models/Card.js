var $ = require('jquery');
var Backbone = require('backbone');
Backbone.$ = $;
var getRandomInt = require('../lib/utils/getRandomInt');

var names = [
    'Frank',
    'Bob',
    'Bill'
];

module.exports = Backbone.Model.extend({
    defaults: {
        attack: 0,
        defence: 0,
        name: ''
    },
    /**
     * Generates a random card, with attack, defence and name attributes.
     */
    generateRandom: function () {
        this.set('attack', getRandomInt(1, 100));
        this.set('defence', getRandomInt(1, 100));
        this.setRandomName();
    },
    /**
     * Selects a name from the `names` array at random
     */
    setRandomName: function () {
        var idx = getRandomInt(1, names.length);
        this.set('name', names[idx - 1]);
    },
    /**
     * Compares a given value to this cards attack attribute, returning 1 if
     * the value to compare is higher, 0 if the values are the saem, or -1 if 
     * the value to compare is lower.
     * @param {Number} toCompare  The value to compare to this card.
     * @return {Number}
     */
    compareAttack: function (toCompare) {
        var attack = this.get('attack');
        if (attack === toCompare) {
            return 0;
        }
        return attack < toCompare ? 1 : -1;
    },
    /**
     * Compares a given value to this cards defence attribute, returning 1 if
     * the value to compare is higher, 0 if the values are the saem, or -1 if 
     * the value to compare is lower.
     * @param {Number} toCompare  The value to compare to this card.
     * @return {Number}
     */
    compareDefence: function (toCompare) {
        var defence = this.get('defence');
        if (defence === toCompare) {
            return 0;
        }
        return defence < toCompare ? 1 : -1;
    },
    toString: function () {
        var rtn = '----------\n' +
            'Name: "' + this.get('name') + '"\n' +
            'Attack: ' + this.get('attack') + '\n' +
            'Defence: ' + this.get('defence') + '\n'
        return rtn;
    }
});
