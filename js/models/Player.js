var $ = require('jquery');
var Backbone = require('backbone');
Backbone.$ = $;
var config = require('../config/config');

module.exports = Backbone.Model.extend({
    defaults: {
        score: 0,
        name: '',
        email: ''
    },
    /**
     * Increases the score by a set amount. If no amount if given, it will use config.pointsPerGuess.
     * @param {Number} amount  The amount to increase the score by. Defaults to config.pointsPerGuess.
     */
    incScore: function (amount) {
        var increase = amount || config.pointsPerGuess;
        var newScore = this.get('score') + increase;
        this.set('score', newScore);
    },
    /**
     * Called when the game resets. Resets the score, and any other non-permanent attributes.
     */
    reset: function () {
        this.set('score', this.defaults.score);
    },
    /**
     * Checks if the user has won the game.
     */
    hasWon: function () {
        return this.get('score') >= (config.pointsPerGuess * config.numberOfCards);
    }
});
