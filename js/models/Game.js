var $ = require('jquery');
var Backbone = require('backbone');
Backbone.$ = $;
var getRandomInt = require('../lib/utils/getRandomInt');

module.exports = Backbone.Model.extend({
    defaults: {
        mode: 0
    },
    initialize: function () {
        this.setRandomMode();
    },
    setRandomMode: function () {
        var idx = getRandomInt(0, 1);
        this.set('mode', idx);
    },
    MODES: {
        ATTACK: 0,
        DEFENCE: 1
    }
});