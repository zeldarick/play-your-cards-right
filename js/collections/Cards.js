var $ = require('jquery');
var Backbone = require('backbone');
Backbone.$ = $;
var Card = require('../models/Card');

module.exports = Backbone.Collection.extend({
    model: Card,
    populate: function (num) {
        // TODO: Pull from an array of cards, instead of generating random cards.
        for (var i = num; i >= 1; i--) {
            var card = new Card();
            card.generateRandom();
            this.add(card);
        }
    }
});
