var $ = require('jquery');

module.exports = {
    /**
     * Displays an overlay over the entire window, with the provided element in the centre.
     * @param {DOMElement} element  The element to display inside the overlay.
     * @param {String} attachPont  A selector to find the element you wish to overlay.
     */
    show: function ($element, attachPoint) {
        var $overlay = $('<div id="overlay"></div>');
        $overlay.css({
            opacity: '0.6',
            background: '#000',
            position: 'absolute',
            width: '100%',
            height: '100%',
            left: 0,
            top: 0
        });

        var $container = $('<div></div>');
        $container.css({
            position: 'absolute',
            left: '50%',
            transform: 'translateX(-50%)'
        });

        $container.append($element);
        $overlay.append($container);
        $(attachPoint).append($overlay);
    },
    /**
     * Hides any and all overlays
     */
    hide: function (attachPoint) {
        $(attachPoint).find('#overlay').remove();
    }
};
