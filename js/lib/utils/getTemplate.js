var $ = require('jquery');
var config = require('../../config/config');

/**
 * Makes an ajax request for the given template, and returns a promise.
 * @param {string} template The template name, including path if it is not in the template root dir
 * @return {Promise}
 */
module.exports = function getTemplate (template) {
    return $('#' + template).html();
};
