module.exports = {
    numberOfCards: 10,
    pointsPerGuess: 1,
    attachPoint: '#content',
    nextScreenDelay: 1000,
    templateRoot: '/templates'
};
