var $ = require('jquery');
var Backbone = require('backbone');
Backbone.$ = $;

module.exports = Backbone.View.extend({
    tagName: 'ul',
    className: 'cards',
    initialize: function (options) {
        this.controller = options.controller;
        this.collection = options.collection;
        this.listenTo(this.collection, 'remove', this.render);
    },
    createCardEl: function (attribs) {
        var $el = $('<li></li>');
        $el.append('Name: ' + attribs.name);
        $el.append('Attack: ' + attribs.attack);
        $el.append('Defence: ' + attribs.defence);
        return $el;
    },
    render: function () {
        console.log('rendering cards');
        var $currentCard = this.createCardEl({
            name: this.controller.currentCard.get('name'),
            attack: this.controller.currentCard.get('attack'),
            defence: this.controller.currentCard.get('defence')
        });

        var $nextCard;
        if (this.controller.cardRevealed) {
            $nextCard = this.createCardEl({
                name: this.controller.nextCard.get('name'),
                attack: this.controller.nextCard.get('attack'),
                defence: this.controller.nextCard.get('defence')
            });
        } else {
            $nextCard = $('<li>Back of card...</li>');
        }

        this.$el.html('');
        this.$el.append($currentCard);
        this.$el.append($nextCard);

        return this;
    },
    append: function ($el) {
        this.$el.append($el);
    },
    revealCard: function () {
        // TODO: Make this append a class to play the animation?
        console.log('card revealed!');
        this.render();
    }
});