var $ = require('jquery');
var Backbone = require('backbone');
Backbone.$ = $;
var config = require('../config/config');

module.exports = Backbone.View.extend({
    tagName: 'div',
    className: 'score',
    initialize: function () {
        this.listenTo(this.model, 'change:score', this.render);
    },
    render: function () {
        var score = this.model.get('score');
        var outOf = config.pointsPerGuess * config.numberOfCards;
        this.$el.html('<p>score: ' + score + ' / ' + outOf + '</p>');
        return this;
    }
});
