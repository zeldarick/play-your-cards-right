var $ = require('jquery');
var Backbone = require('backbone');
Backbone.$ = $;

module.exports = Backbone.View.extend({
    tagName: 'h1',
    className: 'question',
    initialize: function () {
        this.listenTo(this.model, 'change:mode', this.render);
    },
    render: function () {
        var mode = this.model.get('mode');
        var word = mode === this.model.MODES.ATTACK ? 'attack' : 'defence';
        this.$el.html('<p>Is ' + word + ' higher or lower on the next card?</p>');
        return this;
    }
});
