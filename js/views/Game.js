var $ = require('jquery');
var Backbone = require('backbone');
Backbone.$ = $;
var config = require('../config/config');
var overlay = require('../lib/utils/overlay');

module.exports = Backbone.View.extend({
    tagName: 'div',
    className: 'game',
    events: {
        'click .higher': 'clickHigher',
        'click .lower': 'clickLower'
    },
    initialize: function (options) {
        this.controller = options.controller;
    },
    render: function () {
        var $buttons = $('<div id="buttons"></div>');

        var $higher = $('<button class="higher">HIGHER</button>');
        $buttons.append($higher);

        var $lower = $('<button class="lower">LOWER</button>');
        $buttons.append($lower);

        this.$el.html($buttons);

        // Once our main view has rendered, attach any child views
        this.controller.renderSubViews();

        return this;
    },
    clickHigher: function () {
        this.controller.guessHigher();
    },
    clickLower: function () {
        this.controller.guessLower();
    },
    wrongGuess: function () {
        var $el = $('<p>Wrong! Sorry, start again...</p>').css({
            color: 'white'
        });
        overlay.show($el, config.attachPoint);
        this.hideOverlayIn(config.nextScreenDelay);
    },
    correctGuess: function () {
        var $el = $('<p>Correct!</p>').css({
            color: 'white'
        });
        overlay.show($el, config.attachPoint);
        this.hideOverlayIn(config.nextScreenDelay);
    },
    hideOverlayIn: function (delay) {
        setTimeout(function () {
            overlay.hide(config.attachPoint);
        }, delay);
    },
    append: function ($el, selector) {
        if (!selector) {
            this.$el.append($el);
        } else {
            this.$el.find(selector).append($el);
        }
    },
    appendSelf: function () {
        $(config.attachPoint).append(this.render().$el);
    }
});