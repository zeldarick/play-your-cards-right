// Models
var Game = require('../models/Game');
var Player = require('../models/Player');
// Controllers
var ScoreController = require('../controllers/ScoreController');
var CardsController = require('../controllers/CardsController');
var QuestionController = require('../controllers/QuestionController');
// Views
var GameView = require('../views/Game');
// Libs
var config = require('../config/config');

/**
 * The Game Controller... controls the game...
 */
var GameController = function () {
    // Models
    this.game = new Game();
    this.player = new Player();

    // Views
    this.view = new GameView({
        model: this.game,
        controller: this
    });

    // Controllers
    this.cards = new CardsController();
    this.score = new ScoreController(this.player);
    this.question = new QuestionController(this.game);
}

/**
 * Initialise the game, attach to any DOM nodes, etc. Should only be called
 * when the DOM is ready.
 */
GameController.prototype.init = function () {
    this.view.appendSelf();
    this.help();
};

/**
 * Renders any views that attach to our main game view.
 */
GameController.prototype.renderSubViews = function () {
    this.view.append(this.question.view.render().$el);
    this.view.append(this.score.view.render().$el);
    this.view.append(this.cards.view.render().$el);
};

/**
 * Allows the user to register their guess. Can either be 1 for the next card being higher,
 * 0 for the values being the same, or -1 for the next card being lower.
 * @param {Number} guess  The user's guess. 1, 0 or -1.
 */
GameController.prototype.guess = function (guess) {
    // Reveal the next card
    this.cards.reveal();

    // TODO: Disable buttons until ready for next guess.

    var result;
    if (this.game.get('mode') === this.game.MODES.ATTACK) {
        var nextAttack = this.cards.nextCard.get('attack');
        result = this.cards.currentCard.compareAttack(nextAttack);
    } else {
        var nextDefence = this.cards.nextCard.get('defence');
        result = this.cards.currentCard.compareDefence(nextDefence);
    }

    if (result === guess) {
        setTimeout(this.correctGuess.bind(this), config.nextScreenDelay);
    } else {
        setTimeout(this.wrongGuess.bind(this), config.nextScreenDelay);
    }
};

/**
 * Called when the user guesses that the next card will be higher.
 */
GameController.prototype.guessHigher = function () {
    this.guess(1);
};

/**
 * Called when the user guesses that the next card will be lower.
 */
GameController.prototype.guessLower = function () {
    this.guess(-1);
};

/**
 * Called when the user guesses correctly. Also checks if the player has won.
 */
GameController.prototype.correctGuess = function () {
    // Increase the player's score
    this.player.incScore();
    this.view.correctGuess();
    if (this.player.hasWon()) {
        this.won();
    } else {
        this.advanceCard();
    }
};

/**
 * Advances to the next card and sets a random game mode, or if there are no cards left
 * the player loses.
 */
GameController.prototype.advanceCard = function () {
    if (this.cards.cardsLeft() > 0) {
        this.cards.advanceCard();
        // Select another game mode.
        this.game.setRandomMode();
    } else {
        this.noneLeft();
    }
};

/**
 * Called when the user guesses incorrectly.
 */
GameController.prototype.wrongGuess = function () {
    this.view.wrongGuess();
    setTimeout(this.reset.bind(this), config.nextScreenDelay);
};

/**
 * Called when the player wins the game.
 */
GameController.prototype.won = function () {
    console.log('You win!');
};

/**
 * Called if there are no cards left.
 */
GameController.prototype.noneLeft = function () {
    console.log('No cards left, and you didn\'t win... :(');
};

/**
 * Resets the game to the initial state.
 */
GameController.prototype.reset = function () {
    // Reset player score.
    this.player.reset();
    // Reset cards.
    this.cards.reset();
    // Pick a random game mode again.
    this.game.setRandomMode();
};

/**
 * Display a help message in the console showing how to play from the console.
 */
GameController.prototype.help = function () {
    console.log('type PYCR.guessHigher() or PYCR.guessLower().');
};

module.exports = GameController;
