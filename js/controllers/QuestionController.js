var QuestionView = require('../views/Question');

var QuestionController = function (game) {
    this.view = new QuestionView({
        model: game
    });
}

module.exports = QuestionController;
