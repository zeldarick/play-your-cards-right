var ScoreView = require('../views/Score');

var ScoreController = function (player) {
    this.view = new ScoreView({
        model: player
    });
}

module.exports = ScoreController;
