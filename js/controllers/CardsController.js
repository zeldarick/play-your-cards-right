var Cards = require('../collections/Cards');
var CardsView = require('../views/Cards');
var config = require('../config/config');

var CardsController = function () {
    this.collection = new Cards();

    this.init();

    this.view = new CardsView({
        collection: this.collection,
        controller: this
    });
};

/**
 * populates the collection and pops of current and next cards.
 */
CardsController.prototype.init = function () {
    this.cardRevealed = false;
    this.collection.populate(config.numberOfCards);
    this.currentCard = this.collection.pop({silent: true});
    this.nextCard = this.collection.pop();
};

/**
 * Returns how many cards we have left in the collection.
 * @return {Number}
 */
CardsController.prototype.cardsLeft = function () {
    return this.collection.length;
};

/**
 * Advances `currentCard` to the next card, and pop a new `nextCard` from the collection.
 */
CardsController.prototype.advanceCard = function () {
    // Advance to the next card.
    this.cardRevealed = false;
    this.currentCard = this.nextCard;
    this.nextCard = this.collection.pop();
};

/**
 * Resets the collections and gets new cards.
 */
CardsController.prototype.reset = function () {
    this.collection.reset();
    this.init();
};

CardsController.prototype.reveal = function () {
    this.cardRevealed = true;
    this.view.revealCard();
};

module.exports = CardsController;
