var browserify = require('browserify');
var gulp = require('gulp');
var less = require('gulp-less');
var plumber = require('gulp-plumber');
var autoprefixer = require('gulp-autoprefixer');
var source = require('vinyl-source-stream');
var del = require('del');

gulp.task('js', function () {
  var b = browserify({
    debug: true
  });

  return b
    .add('./js/index.js')
    .bundle()
    .pipe(source('bundle.js'))
    .on('error', onError)
    .pipe(gulp.dest('./build/js'));
});

gulp.task('indexhtml', function () {
    gulp.src('./index.html')
    .pipe(gulp.dest('./build'));
});

// gulp.task('templates', function () {
//     gulp.src('./templates/*.mst')
//     .pipe(gulp.dest('./build/templates/'))
// });

gulp.task('css', function () {
    return gulp.src('./css/*.less')
        .on('error', onError)
        .pipe(plumber())
        .pipe(less())
        .pipe(autoprefixer({
            browsers: ['> 1%', 'last 2 versions', 'Firefox ESR', 'Opera 12.1', 'Firefox > 20']
        }))
        .pipe(gulp.dest('./build/css'));
});

gulp.task('clean', function (cb) {
    del(['build/*'], cb)
});

function onError (error) {
    console.log(error);
}

gulp.task('watch', function () {
    gulp.watch('./js/**/*.js', ['js']);
    gulp.watch('./css/*.less', ['css']);
    // gulp.watch('./templates/*.mst', ['templates']);
});

gulp.task('default', ['clean', 'js', 'css', 'indexhtml']);
